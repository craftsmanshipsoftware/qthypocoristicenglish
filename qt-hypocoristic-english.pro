QT = core

include($$PWD/qt-hypocoristic-english.pri)

#VERSION = 1.0

TEMPLATE = lib
TARGET = $$qtLibraryTarget(QtHypocoristicEnglish$$QT_LIBINFIX)
CONFIG += module create_prl
DEFINES+= QT_BUILD_HYPOCORISTIC_ENGLISH_LIB
mac:QMAKE_FRAMEWORK_BUNDLE_NAME = $$TARGET

headersDataFiles.files = $$PWD/qhypocoristic-english.h

# install to Qt installation directory if no PREFIX specified
_PREFIX = $$PREFIX
isEmpty(_PREFIX) {
	INSTALL_HEADER_PATH = $$[QT_INSTALL_HEADERS]/QtHypocoristicEnglish/
	INSTALL_LIB_PATH = $$[QT_INSTALL_LIBS]
} else {

	INSTALL_HEADER_PATH = $$PREFIX/include/QtHypocoristicEnglish/
	INSTALL_LIB_PATH = $$PREFIX/lib
}
message(install to: $$INSTALL_LIB_PATH)
headersDataFiles.path = $$INSTALL_HEADER_PATH
target.path = $$INSTALL_LIB_PATH

INSTALLS+= target headersDataFiles
