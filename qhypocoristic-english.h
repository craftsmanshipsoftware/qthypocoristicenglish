// clang-format off
/**************************************************************************************************
	Copyright (C) 2024  Craftsmanship Software Inc.
	This file is part of QtHypocoristicEnglish

	QtHypocoristicEnglish is free software; you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	QtHypocoristicEnglish is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with QtHypocoristicEnglish; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#ifndef QHYPOCORISTIC_ENGLISH_H
#define QHYPOCORISTIC_ENGLISH_H


#include <QtCore/QtGlobal>

#if (!defined(QT_STATIC) && !defined(QHYPOCORISTIC_ENGLISH_STATIC))
#       ifdef QT_BUILD_HYPOCORISTIC_ENGLISH_LIB
#               define Q_HYPOCORISTIC_ENGLISH_EXPORT Q_DECL_EXPORT
#       else
#               define Q_HYPOCORISTIC_ENGLISH_EXPORT Q_DECL_IMPORT
#       endif
#else
#       define Q_HYPOCORISTIC_ENGLISH_EXPORT
#endif

class QHypocoristicEnglish
{
public:
	static bool check(const QString &a, const QString &b);
};

#endif
