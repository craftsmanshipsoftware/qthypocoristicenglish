// clang-format off
/**************************************************************************************************
	Copyright (C) 2024  Craftsmanship Software Inc.
	This file is part of QtHypocoristicEnglish

	QtHypocoristicEnglish is free software; you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	QtHypocoristicEnglish is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with QtHypocoristicEnglish; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QCoreApplication>
#include <QFile>
#include <QByteArray>
#include <QDebug>

int main(int argc, char *argv[])
{
	QCoreApplication app(argc, argv);
	app.setApplicationName("hypocoristic-list-importer");

	QMap<QString, int> names;
	QList<QStringList> lines;

	QFile inputFile("../hypocoristic-list.txt");
	inputFile.open(QIODevice::ReadOnly);
	QString line;
	do {
		line = inputFile.readLine();
		line = line.toLower();
		line.replace('(', ',');
		line.replace(')', "");
		int mark = line.indexOf('-');
		if (mark < 0)  // end if file or bad line
			break;
		QStringList parts;
		parts+= line.left(mark);
		parts+= line.right(line.length() - mark - 1).trimmed().split(',');

		// check for blank parts (caused by extra comma in line)
		for (int i=0; i<parts.size(); i++) {
			if (parts.at(i).isEmpty())
				parts.removeAt(i);
		}
		lines.append(parts);

		for (auto name : parts) {
			QString trimmed = name.trimmed();
			if (!trimmed.isEmpty() && parts.size() && !names.contains(trimmed)) {
				names.insert(trimmed, 0);
			}
		}

	} while (!line.isEmpty());
	qDebug() << "Total lines" << lines.size();
	qDebug() << "Total names" << names.size();

	int i=0;
	for (QMap<QString, int>::Iterator it = names.begin(); it != names.end(); it++) {
		it.value() = i;
		i++;
	}

	// build cross reference
	QList<int> associations[names.size()];
	for (QStringList line : lines) {
		int numbers[line.size()];

		// look up each number reference for each name on line
		for (int i=0; i<line.size(); i++)
			numbers[i] = names.value(line.at(i).trimmed());

		for (int i=0; i<line.size(); i++) {
			QList<int> cross;
			for (int j=0; j<line.size(); j++) {
				if (i != j)  // don't include self
					cross.append(numbers[j]);
			}
			associations[numbers[i]] = cross;
		}
	}

	// write name list
	QFile outputFile("../hypocoristic-list.h");
	outputFile.open(QIODevice::WriteOnly);

	outputFile.write("#include <QList>\n");
	outputFile.write("#include <QMap>\n\n");

	outputFile.write("QMap<QString, int> nameList = {\n");
	for (QMap<QString, int>::Iterator it = names.begin(); it != names.end(); it++) {
		QString out = "\t{\"" + it.key() + "\", " + QString::number(it.value()) + "},\n";
		outputFile.write(out.toUtf8());
		it.value();
		i++;
	}
	outputFile.write("};\n");

	// write cross reference
	outputFile.write("\n\nQList<int> crossReference[] = {\n");
	for (int i=0; i<names.size(); i++) {
		QList<int> cross = associations[i];
		QString out = "\t{";
		for (int j=0; j<cross.size(); j++) {
			if (j != 0)
				out+= ',';
			out+= QString::number(cross.at(j));
		}
		out+= "},\n";
		outputFile.write(out.toUtf8());
	}
	outputFile.write("};\n");

	outputFile.close();
}
