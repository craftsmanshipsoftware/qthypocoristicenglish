
QHypocoristicEnglish is a small library to assist matching hypocoristic English names.  For example

```c++
QHypocoristicEnglish::check("jim", "james");  // returns true
QHypocoristicEnglish::check("jim", "jimmy");  // returns true
QHypocoristicEnglish::check("jimmy", "james");  // returns true
QHypocoristicEnglish::check("jamie", "james");  // returns false
```

### Updating the vocabulary

The vocabulary is copy and pasted from the [English Hypocoristic list](https://en.wiktionary.org/wiki/Appendix:English_given_names) from wiktionary.  Paste replacing the existing text in hypocoristic-list.txt.  Then build and run the importer.  It takes no parameters.  The importer rebuilds hypocoristic-list.h.  Then rebuild the QtHypocoristicEnglish.

### Building

QHypocoristicEnglish can be built directly into your project if your project is [LGPL3](http://www.gnu.org/licenses/lgpl-3.0.en.html) compatible.  If your project is closed source, you can build QHypocoristicEnglish as a dynamic library and link against it.

#### Building into your project

1. Clone or download QHypocoristicEnglish.  If you download, unzip.
2. Copy the qthypocoristicenglish directory to be under your project's directory.
3. Include the qt-hypocoristic-english.pri file in your projects .pro file

    include(qthypocoristicenglish/qt-hypocoristic-english.pri)

4. Add QHYPOCORISTIC_ENGLISH_STATIC define in your projects .pro file

    DEFINES= QHYPOCORISTIC_ENGLISH_STATIC

#### Compiling as a dynamic library

1. Clone or download QHypocoristicEnglish.  If you download, unzip.
2. Enter the qthypocoristicenglish directory, run qmake and then make.


### Distribution Requirements


Distributing Software / Apps that use QHypocoristicEnglish must follow the requirements of the LGPLv3.  Some of my interpretations of the LGPLv3


* LGPLv3 text or a link to the LGPLv3 text must be distributed with the binary.  My preferred way to do this is in the App's "About" section.
* An offer of source code with a link to QHypocoristicEnglish must be distributed with the binary.  My preferred way to do this is in the App's "About" section
* For Android and iOS apps only, instructions on how to re-link or re-package the App or a link to instructions must be distributed with the binary.  My preferred way to do this is in the App's "About" section.


All of the above must be shown to the user at least once, separate from the EULA, with a method for the user to acknowledge they have seen it (an ok button).  Ideally all of the above is also listed in the description of the App on the App Store.

### Apple App Store deployment

Publishing closed source Apps that use a LGPLv3 library in the Apple App Store must provide a method for the end user to 1. update the library in the app and 2. run the new version of the app with the updated library.  Qt on iOS further complicates this by using static linking.  Closed source Apps on iOS using QHypocoristicEnglish must provide the apps object files along with clear step by step instructions on how to re-link the app with a new / different version of QHypocoristicEnglish (obligation 1).  iOS end uses can run the re-linked App on their device by creating a free iOS developer account and use the time limited signing on that account for their device.  (obligation 2)  I consider this an poor way to meet obligation 2, but as long as Apple has this mechanism, obligation 2 is meet.  I will not pursue copyright infringement as long as the individual / organization is meeting obligation 1 and 2 and the Distribution Requirements above.
