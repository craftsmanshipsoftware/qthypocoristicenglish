// clang-format off
/**************************************************************************************************
	Copyright (C) 2024  Craftsmanship Software Inc.
	This file is part of QtHypocoristicEnglish

	QtHypocoristicEnglish is free software; you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	QtHypocoristicEnglish is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with QtHypocoristicEnglish; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include "hypocoristic-list.h"
#include "qhypocoristic-english.h"

bool QHypocoristicEnglish::check(const QString &a, const QString &b)
{
	bool match = false;
	int aID;
	int bID;

	if (nameList.contains(a) && nameList.contains(b)) {
		aID = nameList.value(a);
		bID = nameList.value(b);
		QList<int> nameIDs = crossReference[aID];
		[&] {
			for (auto x : nameIDs) {
				if (x == bID) {
					match = true;
					return;
				}
			}

			nameIDs = crossReference[bID];
			for (auto x : nameIDs) {
				if (x == aID) {
					match = true;
					return;
				}
			}
		}();
	}

	return match;
}
